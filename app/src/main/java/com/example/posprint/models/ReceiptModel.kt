package com.example.posprint.models

data class ReceiptModel (
        var terminalIDText: String? = null,
        var idText: String? = null,
        var receiptTitleText: String? = null,
        var dateTimeText: String? = null,
        var stanText: String? = null,
        var rrnText: String? = null,
        var balanceValue: String? = null,
        var cardNumberText: String? = null,
        var expiryDateText: String? = null,
        var authCodeText: String? = null,
        var txnStatusText: String? = null,
        var responseCodeText: String? = null,
        var balanceText: String? = null
)