package com.example.posprint.models

class RequestModel (
    val TerminalId: String?,
    val RequestType: Int?,
    val Reference: String?
)