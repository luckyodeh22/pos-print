package com.example.posprint

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.globalaccelerex.globalaccelerexandroidposclientlibrary.GAClientLib
import com.globalaccelerex.globalaccelerexandroidposclientlibrary.printing.FontSize
import com.globalaccelerex.globalaccelerexandroidposclientlibrary.printing.ReceiptFormat
import com.globalaccelerex.globalaccelerexandroidposclientlibrary.printing.TextAlignment
import com.globalaccelerex.globalaccelerexandroidposclientlibrary.util.Countries
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }


}