package com.example.posprint.network

import com.example.posprint.models.RequestModel
import com.example.posprint.models.ResponseModel
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST
import java.util.concurrent.TimeUnit


private const val BASE_URL =
    "https://api.globalaccelerex.com/posvasprint/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

var client: OkHttpClient = OkHttpClient.Builder()
    .connectTimeout(30, TimeUnit.SECONDS)
    .readTimeout(30, TimeUnit.SECONDS).build()

private val retrofit = Retrofit.Builder()
    .client(client)
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()


interface ApiService {
    @POST("api/transactions/details")
    suspend fun getTransactionDetails(
        @Header("TerminalId") terminalId: String,
        @Body requestPara: RequestModel
    ): ResponseModel

}


object TransactionDetailsApi {
    val retrofitService : ApiService by lazy {
        retrofit.create(ApiService::class.java)
    }

}
