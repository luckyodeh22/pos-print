package com.example.posprint.overview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.globalaccelerex.globalaccelerexandroidposclientlibrary.GAClientLib
import com.globalaccelerex.globalaccelerexandroidposclientlibrary.util.Countries
import com.example.posprint.databinding.FragmentOverviewBinding
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class OverviewFragment: Fragment() {

    var requestType: Int? = null

    private val viewModel: OverviewViewModel by viewModels()

    val supportLibrary = GAClientLib.Builder()
            .setCountryCode(Countries.NIGERIA)
            .build()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentOverviewBinding.inflate(inflater)

        // Allows Data Binding to Observe LiveData with the lifecycle of this Fragment
        binding.lifecycleOwner = this

        // Giving the binding access to the OverviewViewModel
        binding.viewModel = viewModel

        binding.btnCheckDetails.setOnClickListener {

            val terminalID = binding.tvTerminalIDText.text.toString()
            val reference = binding.tvReferenceNumText.text.toString()

            if (reference.length == 6){
                requestType = 1
            } else {
                requestType = 0
            }

            viewModel.getDetails(terminalID, requestType!!,reference)
    }

        binding.btnPrint.setOnClickListener {
            supportLibrary.printer.printReceipt(receipt = viewModel.passReceipt().generatePaymentReceipt(), callingComponent = this)
        }
        return binding.root
    }
}
