package com.example.posprint.overview

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import com.example.posprint.network.TransactionDetailsApi
import com.globalaccelerex.globalaccelerexandroidposclientlibrary.printing.FontSize
import com.globalaccelerex.globalaccelerexandroidposclientlibrary.printing.ReceiptFormat
import com.globalaccelerex.globalaccelerexandroidposclientlibrary.printing.TextAlignment
import kotlinx.coroutines.launch
import java.lang.Exception
import com.example.posprint.models.ReceiptModel
import com.example.posprint.models.RequestModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


enum class CallApiStatus { LOADING, ERROR, DONE }

@HiltViewModel
class OverviewViewModel @Inject constructor (application: Application) : AndroidViewModel(application) {

//    val supportLibrary = GAClientLib.Builder()
//            .setCountryCode(Countries.NIGERIA)
//            .build()
    private var receiptFields = ReceiptModel()


//    var receipt: ReceiptFormat? = null
//    private set

    // The internal MutableLiveData that stores the status of the most recent request
    private val _status = MutableLiveData<CallApiStatus>()
    // The external immutable LiveData for the request status
    val status: LiveData<CallApiStatus> = _status

    private val _result = MutableLiveData<String>()
    val result: LiveData<String> = _result

//    private val _receiptResult = MutableLiveData<ResponseData>()
//    var receiptResult: LiveData<ResponseData> = _receiptResult

//    private var progressDialog: Dialog? = null



      fun getDetails(terminalID: String, requestType: Int, reference: String) {
          Log.i("Terminal ID", "$terminalID")
          Log.i("Request type", "${requestType.toString()}")
          Log.i("Reference", "$reference")

          viewModelScope.launch {
              _status.value = CallApiStatus.LOADING
              try {
                val requestParas = RequestModel("$terminalID", requestType, "$reference")
                val listResult =
                        TransactionDetailsApi
                                .retrofitService
                                .getTransactionDetails(
                                        "$terminalID", requestParas)

                  receiptFields.terminalIDText = listResult.TerminalId
                  receiptFields.idText = listResult.id
                  receiptFields.dateTimeText = listResult.de7
                  receiptFields.stanText = listResult.de11
                  receiptFields.rrnText = listResult.de37
                  receiptFields.balanceValue = listResult.de4
                  receiptFields.cardNumberText = listResult.de2

                  receiptFields.expiryDateText = listResult.de14
                  receiptFields.expiryDateText = receiptFields.expiryDateText!!.substring(0, 2) + "/" + receiptFields.expiryDateText!!.substring(2, receiptFields.expiryDateText!!.length);

                  receiptFields.authCodeText = listResult.de38
                  receiptFields.responseCodeText = listResult.de39

                  if(listResult.de1 == "0210" && listResult.de3!!.take(2) == "00"){
                      receiptFields.receiptTitleText = "PURCHASE"
                      receiptFields.balanceText = "Amount"
                  } else if (listResult.de1 == "0210" && listResult.de3!!.take(2) == "01"){
                      receiptFields.receiptTitleText = "CASH ADVANCE"
                      receiptFields.balanceText = "Amount"
                  }

                  if(listResult.de39 == "00"){
                      receiptFields.txnStatusText = "TRANSACTION APPROVED"
                  } else if(listResult.de39 == "03") {
                      receiptFields.txnStatusText = "INVALID MERCHANT"
                  }

                  _result.value = listResult.toString()
                  _status.value = CallApiStatus.DONE

            } catch (e: Exception){
                _result.value = "Failure: ${e.message}"
                  _status.value = CallApiStatus.ERROR
            }
        }
    }

//    private fun showCustomProgressDialog() {
//        progressDialog = Dialog(getApplication())
//
//        progressDialog!!.setContentView(R.layout.dialog_custom_progress)
//
//        progressDialog!!.show()
//    }
//
//    private fun hideProgressDialog() {
//        if (progressDialog != null) {
//            progressDialog!!.dismiss()
//        }
//    }

    fun passReceipt(): ReceiptFormat {
        val receiptFormat = ReceiptFormat(getApplication())
        receiptFormat.addKeyValuePair("Terminal ID", "${receiptFields.terminalIDText}")
        receiptFormat.addLineDivider()
        receiptFormat.addSingleLine("${receiptFields.receiptTitleText}", TextAlignment.ALIGN_CENTER, isBold = true, fontSize = FontSize.LARGE)
        receiptFormat.addKeyValuePair("DATE/TIME", "${receiptFields.dateTimeText}")
        receiptFormat.addKeyValuePair("STAN", "${receiptFields.stanText}")
        receiptFormat.addKeyValuePair("RRN", "${receiptFields.rrnText}")
        receiptFormat.addLineDivider()
        receiptFormat.addKeyValuePair("${receiptFields.balanceText}", "NGN ${receiptFields.balanceValue}")
        receiptFormat.addSingleLine("${receiptFields.cardNumberText}", TextAlignment.ALIGN_CENTER, isBold = true)
        receiptFormat.addKeyValuePair("Expiry Date", "${receiptFields.expiryDateText}")
        receiptFormat.addSpaceDivider()
        receiptFormat.addKeyValuePair("Auth Code", "${receiptFields.authCodeText}")
        receiptFormat.addLineDivider()
        receiptFormat.addSingleLine("${receiptFields.txnStatusText}", TextAlignment.ALIGN_CENTER, isBold = true)
        receiptFormat.addKeyValuePair("Response Code", "${receiptFields.responseCodeText}")
        receiptFormat.addLineDivider()
        receiptFormat.addSingleLine("THANKS FOR COMING", TextAlignment.ALIGN_CENTER)

    return receiptFormat

    }

}
